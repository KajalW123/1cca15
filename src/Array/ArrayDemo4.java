package Array;
import java.util.Scanner;
public class ArrayDemo4 {
    public static void main(String[] args)
    {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter total number of courses");
        int size=sc1.nextInt();
       double sum=0.0;
        double[] marks = new double[size];
        System.out.println("Enter marks");
        for(int a=0;a<size;a++){
            marks[a]=sc1.nextDouble();
        }
        for(int a=0;a<size;a++){
            sum=sum+marks[a];
        }
        double percent=sum/size;
        System.out.println("sum="+sum);
        System.out.println("percentage="+percent);
    }
}
