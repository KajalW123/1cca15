package SeriesPrograms;

public class BuzzNumber1 {
    public static void main(String[] args) {
        for (int i = 10; i < 1000; i++) {
            int a = i;
            if (a % 10 == 7 || a % 7 == 0) {
                System.out.println(i);
            }
        }
    }
}
