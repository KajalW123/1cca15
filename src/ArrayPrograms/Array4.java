package ArrayPrograms;

public class Array4 {
    public static void main(String[] args) {
        char[] arr = {'S', 'T', 'A', 'R'};
        int line=4;
        int star=4;
        for(int i=0;i<line;i++){
            int ch=i;
            for(int j=0;j<star;j++) {
                System.out.print(arr[ch]);
                ch++;
                if (ch > 3) {
                    ch = 0;
                }
            }
            System.out.println();
        }

    }
}
