package ArrayPrograms;

public class Array3 {
    public static void main(String[] args) {
        int[] arr={2,4,6,8,10,12,14,16,18,20};
        int sum=0;
        for (int a : arr) {
            sum = sum + a;
        }
        System.out.println(sum);
        double avg=sum/arr.length;
        System.out.println(avg);
    }
}
