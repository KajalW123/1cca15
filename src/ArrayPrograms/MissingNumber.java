package ArrayPrograms;

public class MissingNumber {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 5};
        int n = arr.length;
        int sum = ((n + 1) * (n + 2) / 2);
        for (int a : arr)
            sum -= a;
        System.out.println("missing number "+sum);
    }

}
