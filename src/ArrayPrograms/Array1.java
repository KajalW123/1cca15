package ArrayPrograms;
public class Array1 {
    public static void main(String[] args) {

        int[] arr = {1, 2, 3, 4,5,6,7};
        //enhanced for loop
        for(int a:arr){
            System.out.print(a+"  ");
        }
    }
}