package PatternProgram;

public class PatternDemo1 {
    public static void main(String[] args)
    {
        int lines=5;
        int star=5;
        for(int i=0;i<lines;i++)
        {
            for(int j=0;j<star;j++)
            {
               System.out.print("*\t");
            }
            System.out.println();
        }
    }
}
