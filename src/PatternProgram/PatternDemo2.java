package PatternProgram;

public class PatternDemo2 {
    public static void main(String[] args) {
        int lines=5;
        int star=5;
        for(int i=0;i<lines;i++)
        {
            for(int j=0;j<star;j++) {
                if (j == 0 || j == 4 || i == 0 || i == 4)
                    System.out.print("*\t");

                else
                    System.out.print(" \t");
            }
            System.out.println();
            }

        }
    }

