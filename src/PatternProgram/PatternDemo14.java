package PatternProgram;

public class PatternDemo14 {
    public static void main(String[] args) {
        int line = 5;
        int star = 1;
        int space = line - 1;
        int num1 = 1;
        for (int i = 0; i < line; i++) {
            for (int k = 0; k < space; k++) {
                System.out.print("\t");
            }
            for (int j = 0; j < star; j++) {
                System.out.print(num1 + "\t");
                num1++;
                if (num1 > 4) {
                    num1 = 1;
                }
            }
            System.out.println();
            star++;
            space--;


        }
    }
}


