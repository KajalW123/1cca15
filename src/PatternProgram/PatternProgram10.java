package PatternProgram;

public class PatternProgram10 {
    public static void main(String[] args) {
        int star=1;
        int lines=5;
        int space=lines-1;
        for(int i=0;i<lines;i++)
        {
            for(int k=0;k<space;k++)
            {
                System.out.print("   ");
            }
            for(int j=0;j<star;j++){
                System.out.print(" * ");
            }
            System.out.println();
            space--;
            star++;
        }
    }
}
