package NumberPattern;

public class NumberPatternDemo2 {
    public static void main(String[] args) {
        int line=5;
        int number=5;
        for(int i=0;i<line;i++)
        {
            int ch=1;
            for(int j=0;j<number;j++)
            {
                System.out.print( ch+"\t");
                ch++;
            }
            System.out.println();
        }
    }
}

