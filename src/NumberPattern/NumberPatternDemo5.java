package NumberPattern;

public class NumberPatternDemo5 {
    public static void main(String[] args){
        int line=5;
        int star=5;
       int ch=1;
        for(int i=0;i<line;i++)
        {
            for(int j=0;j<star;j++)
            {
                if(i%2==0)
                    System.out.print(ch+"\t");
                else
                System.out.print("*\t");
            }
            System.out.println();
            ch++;
        }
    }
}
