package NumberPattern;

public class NumberPatternDemo1 {
    public static void main(String[] args) {
        int line=5;
        int number=5;
        int ch=1;
        for(int i=0;i<line;i++)
        {
           // int ch=1   for printing the 1 multiple time
            for(int j=0;j<number;j++)
            {
                System.out.print( ch+"\t");
            }
            System.out.println();
            ch++;
        }
    }
}
