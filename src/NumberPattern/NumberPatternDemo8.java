package NumberPattern;

public class NumberPatternDemo8 {
    public static void main(String[] args) {
        int lines=5;
        int star=5;
        for(int i=0;i<lines;i++)
        {
            int ch=1;
            for(int j=0;j<star;j++){
                if(j%2==0){
                    System.out.print(ch);
                    ch++;
                }
                else
                {
                    System.out.print(" * ");

                }
            }
            System.out.println();

        }
    }



}
