package NumberPattern;

public class NumberPatternDemo3 {
    public static void main(String[] args) {
        int line=5;
        int number=5;
        int ch;
        for(int i=0;i<line;i++)
        {
            ch=i+1;
            for(int j=0;j<number;j++)
            {
                System.out.print( ch+"\t");
                ch++;
            }
            System.out.println();
        }
    }
}
