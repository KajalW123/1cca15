package NumberPattern;

public class NumberPatternDemo9 {
    public static void main(String[] args) {
        int lines=5;
        int star=6;
        for(int i=0;i<lines;i++){
            int ch=1;
            for(int j=0;j<star;j++){
                if(j%2==0){
                    System.out.print(ch);
                }
                else{
                    System.out.print(" * ");
                }
               ch++;
            }
            System.out.println();
        }

    }
}
