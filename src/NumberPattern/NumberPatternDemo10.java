package NumberPattern;

public class NumberPatternDemo10 {
    public static void main(String[] args) {
        int line = 9;
        int space = line - 1;
        int star = 1;
        for (int i = 0; i < line; i++) {
            char ch ='A';
            for (int k = 0; k < space; k++) {
                System.out.print("    ");
            }
            for (int j = 0; j < star; j++) {
                System.out.print(ch+"\t\t");
                ch++;
            }

            System.out.println();
            if(i<=3) {
                space--;
                star++;
            }
            else {
                space++;
                star--;
            }
        }
    }
 }
