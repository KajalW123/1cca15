package SortingPrograms;

public class BubbleSort {
    public static void main(String[] args) {
        int[] arr={5,3,6,2,1};
        for(int i=0;i<arr.length;i++){
            for(int j=0;j<arr.length-1-i;j++){  // -i is for reduce the no of iterations
                if(arr[j]>arr[j+1]){
                    int temp=arr[j];
                    arr[j]=arr[j+1];
                    arr[j+1]=temp;
                }
            }

            }
        for(int a:arr){
            System.out.println(a);
        }

    }
}
