package PracticePdf;

public class Program5 {
    public static void main(String[] args) {
        int lines=9;
        int star=5;
        for(int i=0;i<lines;i++){
            for(int j=0;j<star;j++){
                System.out.print("*");
            }
            System.out.println();
            if(i<=3){
                star--;
            }else
            {
                star++;
            }
        }
    }
}
